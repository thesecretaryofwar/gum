import gi

gi.require_version('Gtk', '3.0')

from gi.repository import GObject, GLib, Gio, Gdk, Gtk

from tablabel import TabLabel

@Gtk.Template(filename='ui/tabbar.ui')
class TabBar(Gtk.ScrolledWindow):
    __gtype_name__ = 'TabBar'

    tab_box = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.tab_map = {}
        self.order = []
        self.current = None
        self.multi_view = None

        self.get_hscrollbar().set_visible(False)
        self.connect('scroll-event', self.on_scroll_event)

    def on_scroll_event(self, bar, event):
        hadj = self.get_hadjustment()
        x = hadj.get_value()
        xp = x+10*event.delta_y
        hadj.set_value(xp)

    def set_multiview(self, multi_view):
        self.multi_view = multi_view

    def add_tab(self, view, text, select=False):
        self.multi_view.add(view)

        tab = TabLabel(text)
        self.tab_box.add(tab)

        self.tab_map[view] = tab

        tab.name_event.connect('button-press-event', self.on_tab_clicked, view)
        tab.close_button.connect('clicked', self.on_close_clicked, view)

        if select:
            self.select_tab(view)

    def remove_tab(self, view):
        if view not in self.tab_map:
            return

        tab = self.tab_map[view]
        self.tab_map.pop(view)

        self.order.remove(view)
        if self.current == view:
            self.current = None
            if len(self.order) > 0:
                pview = self.order[-1]
            elif len(self.tab_map) > 0:
                pview = next(reversed(self.tab_map.keys()))
            else:
                pview = None
            if pview is not None:
                self.select_tab(pview)

        self.multi_view.remove(view)
        self.tab_box.remove(tab)

    def select_tab(self, view):
        if view not in self.tab_map:
            return

        if self.current is not None:
            ctab = self.tab_map[self.current]
            cctx = ctab.get_style_context()
            cctx.remove_class('selected')

        tab = self.tab_map[view]
        ctx = tab.get_style_context()
        ctx.add_class('selected')

        self.multi_view.set_visible_child(view)
        self.current = view

        if view in self.order:
            self.order.remove(view)
        self.order.append(view)

    def on_tab_clicked(self, tab, event, view):
        self.select_tab(view)

    def on_close_clicked(self, button, view):
        self.remove_tab(view)
