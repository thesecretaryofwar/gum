import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gdk, Gtk, Gio

from window import Window

class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='org.example.Gum', flags=Gio.ApplicationFlags.FLAGS_NONE)

        # use dark theme
        settings = Gtk.Settings.get_default()
        settings.set_property("gtk-application-prefer-dark-theme", True)

        # global style
        provider = Gtk.CssProvider()
        provider.load_from_path('window.css')

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = Window(application=self)
        win.show_all()
        win.present()
