import gi

gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '4')

from gi.repository import GObject, GLib, Gio, Gdk, Gtk, GtkSource

GObject.type_register(GtkSource.View)

@Gtk.Template(filename='ui/view.ui')
class View(Gtk.ScrolledWindow):
    __gtype_name__ = 'View'

    source_view = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.css_provider = Gtk.CssProvider()
        self.css_provider.load_from_data(b'.source_view { font: 13px Monospace; }')
        source_css = self.source_view.get_style_context()
        source_css.add_provider(self.css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER)

        self.source_buffer = self.source_view.get_buffer()

        # editor styling
        self.source_view.set_highlight_current_line(True)
        self.source_view.set_bottom_margin(150)

        # style scheme
        style_man = GtkSource.StyleSchemeManager.get_default()
        style_man.set_search_path(['styles'])
        style_ids = style_man.get_scheme_ids()
        style_obv = style_man.get_scheme('atom')
        self.source_buffer.set_style_scheme(style_obv)

        # syntax highlighting
        lang_man = GtkSource.LanguageManager.get_default()
        lang_ids = lang_man.get_language_ids()
        lang_py = lang_man.get_language('python3')
        self.source_buffer.set_language(lang_py)

    def open_file(self, fileuri):
        sfile = GtkSource.File()
        gfile = Gio.File.new_for_uri(fileuri)
        sfile.set_location(gfile)

        loader = GtkSource.FileLoader.new(self.source_buffer, sfile)
        loader.load_async(GLib.PRIORITY_DEFAULT, None, None, None, None, None)
