from datetime import datetime
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_msearch import Search
from sqlalchemy.sql import func
from whoosh import qparser, query

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['MSEARCH_ENABLE'] = True
app.config['MSEARCH_BACKEND'] = 'whoosh'
db = SQLAlchemy(app)

search = Search(app=app, db=db)

# create schema

class Paragraph(db.Model):
    __tablename__ = 'paragraph'
    __searchable__ = ['text']
    __msearch_primary_key__ = 'rid'

    pid = db.Column(db.Integer)
    rid = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(), nullable=False)

    def _parser(fieldnames, schema, group, **kwargs):
        parser = qparser.MultifieldParser(fieldnames, schema, group=group, **kwargs)
        parser.add_plugin(qparser.FuzzyTermPlugin())
        return parser
    __msearch_parser__ = _parser

    def __repr__(self):
        return f'{self.pid}/{self.rid}: {self.text}'

# initialize db

db.create_all()
search.create_index()

# test populate

par0 = Paragraph(pid=0, text='Abraham Lincoln')
par2 = Paragraph(pid=1, text='George Washington')

db.session.add_all([par0, par1, par2])
db.session.commit()

# print out database

for p in db.session.query(Paragraph).all():
    print(p)

pmax = db.session.query(func.max(Paragraph.pid)).scalar()
print(pmax)

print(Paragraph.query.msearch(f'Abrah~3 Lincon~3', fields=['text']).all())
