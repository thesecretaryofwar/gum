import gi

gi.require_version('Gtk', '3.0')

from gi.repository import GObject, GLib, Gio, Gdk, Gtk

@Gtk.Template(filename='ui/tablabel.ui')
class TabLabel(Gtk.Box):
    __gtype_name__ = 'TabLabel'

    name_event = Gtk.Template.Child()
    name_label = Gtk.Template.Child()
    close_button = Gtk.Template.Child()

    def __init__(self, text, **kwargs):
        super().__init__(**kwargs)

        self.name_label.set_text(text)

        self.name_event.connect('enter-notify-event', self.on_mouse_enter)
        self.name_event.connect('leave-notify-event', self.on_mouse_leave)

    def on_mouse_enter(self, name, event):
        ctx = self.get_style_context()
        ctx.add_class('hover')

    def on_mouse_leave(self,name, event):
        ctx = self.get_style_context()
        ctx.remove_class('hover')
