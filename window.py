import os
import gi

gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '4')

from gi.repository import GObject, GLib, Gio, Gdk, Gtk, GtkSource

from view import View
from tablabel import TabLabel
from tabbar import TabBar
from multiview import MultiView

GObject.type_register(GtkSource.View)

@Gtk.Template(filename='ui/window.ui')
class Window(Gtk.ApplicationWindow):
    __gtype_name__ = 'Window'

    header_bar = Gtk.Template.Child()
    multi_view = Gtk.Template.Child()
    open_button = Gtk.Template.Child()
    recent_chooser = Gtk.Template.Child()
    tab_bar = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # hook up multiview
        self.tab_bar.set_multiview(self.multi_view)

        # modify recent chooser
        for cont in self.open_button.get_children():
            for button in cont.get_children():
                ctx = button.get_style_context()
                ctx.remove_class('flat')

        # filter recent files
        recent_filter = Gtk.RecentFilter()
        recent_filter.add_mime_type('text/plain')
        self.recent_chooser.set_filter(recent_filter)
        self.recent_chooser.connect('item-activated', self.recent_activated)

        # test files (hacky)
        mod_dir = os.path.abspath(os.path.dirname(__file__))
        self.open_file(f'file://{mod_dir}/testing/test.py')
        self.open_file(f'file://{mod_dir}/testing/test_long.py')

    def recent_activated(self, recent):
        fileuri = recent.get_current_uri()
        self.open_file(fileuri)

    def open_file(self, fileuri):
        fdir, fname = os.path.split(fileuri)

        view = View()
        view.open_file(fileuri)

        label = TabLabel(fname)
        label.close_button.connect('clicked', self.on_close_button, view)

        self.tab_bar.add_tab(view, fname, select=True)

    def on_close_button(self, button, view):
        page = self.notebook.page_num(view)
        self.notebook.remove_page(page)

    @Gtk.Template.Callback()
    def on_button_pressed(self, button):
        choose = Gtk.FileChooserDialog(
            'Choose a file',
            self,
            Gtk.FileChooserAction.OPEN,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.OK,
            ),
        )

        ret = choose.run()

        if ret == Gtk.ResponseType.OK:
            fileuri = choose.get_uri()
        elif ret == Gtk.ResponseType.CANCEL:
            fileuri = None

        choose.destroy()

        if fileuri is not None:
            self.open_file(fileuri)
