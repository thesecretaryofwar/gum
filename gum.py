import sys
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

from app import Application

app = Application()
app.run(sys.argv)
