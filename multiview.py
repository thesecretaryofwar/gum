import gi

gi.require_version('Gtk', '3.0')

from gi.repository import GObject, GLib, Gio, Gdk, Gtk

from tablabel import TabLabel

@Gtk.Template(filename='ui/multiview.ui')
class MultiView(Gtk.Stack):
    __gtype_name__ = 'MultiView'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
